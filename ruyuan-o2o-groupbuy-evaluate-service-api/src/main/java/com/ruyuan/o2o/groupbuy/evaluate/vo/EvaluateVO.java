package com.ruyuan.o2o.groupbuy.evaluate.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 评价服务VO类
 *
 * @author ming qian
 */
@ApiModel(value = "评价服务VO类")
@Getter
@Setter
@ToString
public class EvaluateVO implements Serializable {
    private static final long serialVersionUID = 5885705882605486681L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 商品id
     */
    @ApiModelProperty("商品id")
    private Integer itemId;

    /**
     * 评价等级 1 一星 2 二星 3 三星 4 四星 5 五星
     */
    @ApiModelProperty("评价等级 1 一星 2 二星 3 三星 4 四星 5 五星")
    private Integer level;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;
}