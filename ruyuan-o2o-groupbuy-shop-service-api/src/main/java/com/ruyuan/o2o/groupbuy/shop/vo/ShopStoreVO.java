package com.ruyuan.o2o.groupbuy.shop.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 商户操作门店VO类
 *
 * @author ming qian
 */
@ApiModel(value = "商户操作门店VO类")
@Getter
@Setter
@ToString
public class ShopStoreVO implements Serializable {
    private static final long serialVersionUID = -4756339288035711707L;

    /**
     * 门店id
     */
    @ApiModelProperty("门店id")
    private Integer storeId;

    /**
     * 商户id
     */
    @ApiModelProperty("商户id")
    private Integer shopId;

    /**
     * 门店名称
     */
    @ApiModelProperty("门店名称")
    private String storeName;

    /**
     * 门店宣传
     */
    @ApiModelProperty("门店宣传")
    private String storeDesc;

    /**
     * 门店审核状态 0 已创建 1 审核通过 2 审核拒绝
     */
    @ApiModelProperty("门店审核状态 0 已创建 1 审核通过 2 审核拒绝")
    private Integer checkStatus;

    /**
     * 销售状态 0 未上架 1 已上架 2 已下架
     */
    @ApiModelProperty("销售状态 0 未上架 1 已上架 2 已下架")
    private Integer saleStatus;

    /**
     * 门店联系电话
     */
    @ApiModelProperty("门店联系电话")
    private String storePhone;

    /**
     * 门店icon图片地址
     */
    @ApiModelProperty("门店icon图片地址")
    private String icon;

    /**
     * 省id
     */
    @ApiModelProperty("省id")
    private Integer provinceId;

    /**
     * 市id
     */
    @ApiModelProperty("市id")
    private Integer cityId;

    /**
     * 区id
     */
    @ApiModelProperty("区id")
    private Integer districtId;

    /**
     * 门店地址
     */
    @ApiModelProperty("门店地址")
    private String storeAddress;

    /**
     * 创建时间
     */
    @ApiModelProperty("创建时间")
    private String createTime;

    /**
     * 创建人
     */
    @ApiModelProperty("创建人")
    private Integer createOper;

    /**
     * 更新时间
     */
    @ApiModelProperty("更新时间")
    private String updateTime;

    /**
     * 更新人
     */
    @ApiModelProperty("更新人")
    private Integer updateOper;

    /**
     * 删除标记 0 未删除 1 已删除
     */
    @ApiModelProperty("删除标记 0 未删除 1 已删除")
    private Integer delFlag;
}
