package com.ruyuan.o2o.groupbuy.shop.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 商户服务操作的门店实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class ShopStoreModel implements Serializable {
    private static final long serialVersionUID = 9130971829976672956L;

    /**
     * 门店id
     */
    private Integer storeId;

    /**
     * 商户id
     */
    private Integer shopId;

    /**
     * 门店名称
     */
    private String storeName;

    /**
     * 门店宣传
     */
    private String storeDesc;

    /**
     * 门店审核状态 0 已创建 1 审核通过 2 审核拒绝
     */
    private Integer checkStatus;

    /**
     * 销售状态 0 未上架 1 已上架 2 已下架
     */
    private Integer saleStatus;

    /**
     * 门店联系电话
     */
    private String storePhone;

    /**
     * 门店icon图片地址
     */
    private String icon;

    /**
     * 省id
     */
    private Integer provinceId;

    /**
     * 市id
     */
    private Integer cityId;

    /**
     * 区id
     */
    private Integer districtId;

    /**
     * 门店地址
     */
    private String storeAddress;

    /**
     * 创建时间
     */
    private String createTime;

    /**
     * 创建人
     */
    private Integer createOper;

    /**
     * 更新时间
     */
    private String updateTime;

    /**
     * 更新人
     */
    private Integer updateOper;

    /**
     * 删除标记 0 未删除 1 已删除
     */
    private Integer delFlag;
}
