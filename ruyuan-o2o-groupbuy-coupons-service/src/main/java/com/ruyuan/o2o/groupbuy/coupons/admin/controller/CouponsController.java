package com.ruyuan.o2o.groupbuy.coupons.admin.controller;

import com.ruyuan.o2o.groupbuy.coupons.service.CouponsService;
import com.ruyuan.o2o.groupbuy.coupons.vo.CouponsVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 优惠券管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/coupons")
@Slf4j
@Api(tags = "优惠券管理")
public class CouponsController {

    @Reference(version = "1.0.0", interfaceClass = CouponsService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private CouponsService couponsService;

    /**
     * 创建优惠券
     *
     * @param couponsVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("创建优惠券")
    public String save(@RequestBody CouponsVO couponsVO) {
        if (!couponsService.save(couponsVO)) {
            return "failed";
        }
        log.info("创建优惠券:{} 完成", couponsVO.getCouponsName());
        return "success";
    }

    /**
     * 分页查询优惠券列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询优惠券列表")
    public List<CouponsVO> page(Integer pageNum, Integer pageSize) {
        List<CouponsVO> couponsVoS = couponsService.listByPage(pageNum, pageSize);
        return couponsVoS;
    }
}
