package com.ruyuan.o2o.groupbuy.overage.admin.controller;

import com.ruyuan.o2o.groupbuy.overage.service.OverageService;
import com.ruyuan.o2o.groupbuy.overage.vo.OverageVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 接收充值服务的增删改查请求
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/overage")
@Slf4j
@Api(tags = "充值管理")
public class OverageController {

    @Reference(version = "1.0.0", interfaceClass = OverageService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private OverageService overageService;

    /**
     * 充值余额
     *
     * @param overageVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("充值管理")
    public Boolean save(@RequestBody OverageVO overageVO) {
        overageService.save(overageVO);
        log.info("创建充值完成");
        return Boolean.TRUE;
    }

}
