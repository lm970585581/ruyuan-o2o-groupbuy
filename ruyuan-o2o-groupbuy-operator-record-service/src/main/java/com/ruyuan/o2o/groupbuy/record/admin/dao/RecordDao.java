package com.ruyuan.o2o.groupbuy.record.admin.dao;

import com.ruyuan.o2o.groupbuy.record.model.RecordModel;
import org.springframework.stereotype.Repository;

/**
 * 消费者用户操作记录服务服务DAO
 *
 * @author ming qian
 */
@Repository
public interface RecordDao {
    /**
     * 保存操作记录
     *
     * @param recordModel
     */
    void save(RecordModel recordModel);
}