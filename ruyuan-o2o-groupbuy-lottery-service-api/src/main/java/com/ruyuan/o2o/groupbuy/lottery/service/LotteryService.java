package com.ruyuan.o2o.groupbuy.lottery.service;

import com.ruyuan.o2o.groupbuy.lottery.vo.LotteryVO;

/**
 * 抽奖服务service组件接口
 *
 * @author ming qian
 */
public interface LotteryService {
    /**
     * 用户抽奖
     * 增加用户抽奖记录
     *
     * @param lotteryVO
     */
    void save(LotteryVO lotteryVO);
}
