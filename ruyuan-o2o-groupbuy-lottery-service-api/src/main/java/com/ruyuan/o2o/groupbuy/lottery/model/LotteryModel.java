package com.ruyuan.o2o.groupbuy.lottery.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 抽奖服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class LotteryModel implements Serializable {
    private static final long serialVersionUID = 1393948521982179632L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 消费者用户id
     */
    private Integer userId;

    /**
     * 支付id
     */
    private Integer payId;

    /**
     * 抽奖商品id
     */
    private Integer itemId;

    /**
     * 抽奖所在门店id
     */
    private Integer storeId;

    /**
     * 抽奖结果 0 未中奖 1 中奖
     */
    private Integer lotteryResult;

    /**
     * 抽奖时间
     */
    private String createTime;

}