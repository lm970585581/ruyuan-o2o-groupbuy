package com.ruyuan.o2o.groupbuy.promotions.admin.dao;

import com.ruyuan.o2o.groupbuy.promotions.model.PromotionsModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 活动服务DAO
 *
 * @author ming qian
 */
@Repository
public interface PromotionsDao {

    /**
     * 根据id查询活动
     *
     * @param promotionId 活动id
     * @return
     */
    PromotionsModel findById(Integer promotionId);

    /**
     * 创建活动
     *
     * @param promotionsModel
     */
    void save(PromotionsModel promotionsModel);

    /**
     * 更新活动
     *
     * @param promotionsModel
     */
    void update(PromotionsModel promotionsModel);

    /**
     * 删除活动
     *
     * @param promotionId
     */
    void delete(Integer promotionId);

    /**
     * 分页查询活动
     *
     * @return
     */
    List<PromotionsModel> listByPage();

    /**
     * 查询当前运行中活动
     *
     * @return
     */
    PromotionsModel findRunningPromotion();

}