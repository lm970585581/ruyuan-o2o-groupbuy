package com.ruyuan.o2o.groupbuy.promotions.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 活动服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class PromotionsModel implements Serializable {
    private static final long serialVersionUID = -518899726327752869L;

    /**
     * 活动id
     */
    private Integer promotionId;
    /**
     * 所属门店id
     */
    private Integer storeId;
    /**
     * 所属商户id
     */
    private Integer shopId;
    /**
     * 活动名称
     */
    private String promotionName;
    /**
     * 活动宣传描述
     */
    private String promotionDesc;
    /**
     * 活动类型 0满减 1折扣 2红包返现
     */
    private Integer promotionType;
    /**
     * 活动状态 0已创建 1运行中 2已结束 3已中止
     */
    private Integer promotionStatus;
    /**
     * 满减活动满足金额
     */
    private Double fullPrice;
    /**
     * 满减活动扣减金额
     */
    private Double deductPrice;
    /**
     * 折扣活动消费金额打折比例
     */
    private Double discountPrice;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 创建人
     */
    private Integer createOper;
    /**
     * 删除标记 0 未删除 1 已删除
     */
    private Integer delFlag;
}