package com.ruyuan.o2o.groupbuy.account.admin.controller;

import com.ruyuan.o2o.groupbuy.account.vo.AuthVO;
import com.ruyuan.o2o.groupbuy.account.service.AuthService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户权限
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/auth")
@Slf4j
@Api(tags = "商户权限")
public class AuthController {

    @Autowired
    private AuthService authService;

    /**
     * 创建权限
     *
     * @param authVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("创建权限")
    public Boolean save(@RequestBody AuthVO authVO) {
        authService.save(authVO);
        log.info("权限创建完成");
        return Boolean.TRUE;
    }

    /**
     * 分页查询权限列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询权限列表")
    public List<AuthVO> page(Integer pageNum, Integer pageSize) {
        List<AuthVO> authVoS = authService.listByPage(pageNum, pageSize);
        return authVoS;
    }
}
