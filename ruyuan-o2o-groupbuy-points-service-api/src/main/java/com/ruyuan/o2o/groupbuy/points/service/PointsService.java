package com.ruyuan.o2o.groupbuy.points.service;

import com.ruyuan.o2o.groupbuy.points.vo.PointsVO;

import java.util.List;

/**
 * 积分服务service组件接口
 *
 * @author ming qian
 */
public interface PointsService {
    /**
     * 创建积分
     *
     * @param pointsVO
     */
    void save(PointsVO pointsVO);

    /**
     * 分页查询积分
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<PointsVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 根据用户id查询积分
     *
     * @param userId 用户id
     * @return 积分vo
     */
    PointsVO findByUserId(Integer userId);
}
