package com.ruyuan.o2o.groupbuy.account.service;

import com.ruyuan.o2o.groupbuy.account.vo.AccountRoleVO;
import com.ruyuan.o2o.groupbuy.account.vo.AccountVO;

import java.util.List;

/**
 * 账号管理service组件接口
 *
 * @author ming qian
 */
public interface AccountService {
    /**
     * 创建账号
     *
     * @param accountVO 参数vo
     */
    void save(AccountVO accountVO);

    /**
     * 更新账号
     *
     * @param accountVO 参数vo
     */
    void update(AccountVO accountVO);

    /**
     * 关闭账号
     *
     * @param adminId 账号id
     * @return 返回结果
     */
    Boolean close(Integer adminId);

    /**
     * 开启账号
     *
     * @param adminId 账号id
     * @return 返回结果
     */
    Boolean open(Integer adminId);

    /**
     * 分页查询账号
     *
     * @param pageNum  查询页数
     * @param pageSize 每次查询数
     * @return 查询列表
     */
    List<AccountVO> listByPage(Integer pageNum, Integer pageSize);

    /**
     * 根据id查询账号
     *
     * @param adminId 账号id
     * @return 账号vo
     */
    AccountVO findById(Integer adminId);

    /**
     * 用户绑定角色
     *
     * @param accountRoleVO 前台表单
     * @return 绑定结果
     */
    boolean accountBindRole(AccountRoleVO accountRoleVO);

}
