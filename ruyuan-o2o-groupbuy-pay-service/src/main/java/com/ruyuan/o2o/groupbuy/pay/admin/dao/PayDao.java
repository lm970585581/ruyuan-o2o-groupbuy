package com.ruyuan.o2o.groupbuy.pay.admin.dao;

import com.ruyuan.o2o.groupbuy.pay.model.PayModel;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * 支付信息服务DAO
 *
 * @author ming qian
 */
@Repository
public interface PayDao {
    /**
     * 生成支付信息
     *
     * @param payModel
     */
    void save(PayModel payModel);

    /**
     * 分页查询支付流水信息
     *
     * @return
     */
    List<PayModel> listByPage();
}