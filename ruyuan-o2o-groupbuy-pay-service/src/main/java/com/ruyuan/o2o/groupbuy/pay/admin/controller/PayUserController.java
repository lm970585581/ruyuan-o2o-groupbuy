package com.ruyuan.o2o.groupbuy.pay.admin.controller;

import com.ruyuan.o2o.groupbuy.pay.service.PayUserService;
import com.ruyuan.o2o.groupbuy.pay.vo.PayUserVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户消费记录管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/pay")
@Slf4j
@Api(tags = "用户消费记录管理")
public class PayUserController {

    @Reference(version = "1.0.0", interfaceClass = PayUserService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private PayUserService payUserService;

    /**
     * 查询用户消费记录列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/payPage")
    @ApiOperation("查询用户消费记录列表")
    public List<PayUserVO> page(Integer pageNum, Integer pageSize) {
        List<PayUserVO> payUserVoS = payUserService.listByPage(pageNum, pageSize);
        return payUserVoS;
    }

}
