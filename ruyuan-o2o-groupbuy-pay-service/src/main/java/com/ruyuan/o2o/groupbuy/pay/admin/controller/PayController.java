package com.ruyuan.o2o.groupbuy.pay.admin.controller;

import com.ruyuan.o2o.groupbuy.pay.service.PayService;
import com.ruyuan.o2o.groupbuy.pay.vo.PayVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 支付管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/pay")
@Slf4j
@Api(tags = "支付管理")
public class PayController {

    @Reference(version = "1.0.0", interfaceClass = PayService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private PayService payService;

    /**
     * 分页查询支付信息
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询支付信息列表")
    public List<PayVO> page(Integer pageNum, Integer pageSize) {
        List<PayVO> payVoS = payService.listByPage(pageNum, pageSize);
        return payVoS;
    }

}
