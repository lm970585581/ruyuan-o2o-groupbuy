package com.ruyuan.o2o.groupbuy.lottery.admin.service;

import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.lottery.admin.dao.LotteryDao;
import com.ruyuan.o2o.groupbuy.lottery.model.LotteryModel;
import com.ruyuan.o2o.groupbuy.lottery.service.LotteryService;
import com.ruyuan.o2o.groupbuy.lottery.vo.LotteryVO;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * 处理抽奖服务的增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = LotteryService.class, cluster = "failfast", loadbalance = "roundrobin")
@Slf4j
public class LotteryServiceImpl implements LotteryService {

    @Autowired
    private LotteryDao lotteryDao;

    /**
     * 用户抽奖
     * 增加用户抽奖记录
     *
     * @param lotteryVO
     */
    @Override
    public void save(LotteryVO lotteryVO) {
        //  抽奖算法
        //  计算出抽奖礼品,抽奖概率及奖品等由商户创建抽奖礼品时设置
        log.info("用户：{} 抽取奖品:{} ", lotteryVO.getUserId(), lotteryVO.getItemId());
        //  保存用户抽奖结果
        LotteryModel lotteryModel = new LotteryModel();
        BeanMapper.copy(lotteryVO, lotteryModel);
        lotteryModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        lotteryDao.save(lotteryModel);
    }
}
