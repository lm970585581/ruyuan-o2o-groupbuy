package com.ruyuan.o2o.groupbuy.excel.admin.service;

import com.alibaba.excel.EasyExcel;
import com.ruyuan.o2o.groupbuy.common.Excel;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.excel.service.ExcelService;
import com.ruyuan.o2o.groupbuy.order.dto.OrderExcelDTO;
import com.ruyuan.o2o.groupbuy.order.service.OrderService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;

/**
 * 处理报表服务的service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = ExcelService.class, cluster = "failfast", loadbalance = "roundrobin")
public class ExcelServiceImpl implements ExcelService {

    @Reference(version = "1.0.0", interfaceClass = OrderService.class, cluster = "failfast", loadbalance = "roundrobin")
    private OrderService orderService;

    /**
     * 生成报表
     */
    @Override
    public void excel() {
        String fileName = Excel.EXCEL_PATH
                + TimeUtil.formatExcel(System.currentTimeMillis()) + Excel.EXCEL_SUFFIX;

        EasyExcel.write(fileName, OrderExcelDTO.class).sheet("订单统计表").doWrite(orderService.selectAll());
    }
}
