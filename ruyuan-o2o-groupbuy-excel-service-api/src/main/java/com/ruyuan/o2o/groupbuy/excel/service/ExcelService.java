package com.ruyuan.o2o.groupbuy.excel.service;

/**
 * 报表管理service组件接口
 *
 * @author ming qian
 */
public interface ExcelService {
    /**
     * 生成报表
     *
     * @param
     */
    void excel();
}
