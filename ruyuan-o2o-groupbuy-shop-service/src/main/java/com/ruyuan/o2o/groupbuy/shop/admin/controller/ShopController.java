package com.ruyuan.o2o.groupbuy.shop.admin.controller;

import com.ruyuan.o2o.groupbuy.shop.service.ShopService;
import com.ruyuan.o2o.groupbuy.shop.vo.ShopStoreVO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 商户管理
 *
 * @author ming qian
 */
@RestController
@RequestMapping("/shop")
@Slf4j
@Api(tags = "商户管理")
public class ShopController {

    @Reference(version = "1.0.0", interfaceClass = ShopService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private ShopService shopService;

    /**
     * 创建门店
     *
     * @param shopStoreVO 前台表单
     * @return
     */
    @PostMapping("/save")
    @ApiOperation("创建门店")
    public String save(@RequestBody ShopStoreVO shopStoreVO) {
        if (!shopService.save(shopStoreVO)) {
            return "failed";
        }
        log.info("创建门店:{} 完成", shopStoreVO.getStoreName());
        return "success";
    }

    /**
     * 更新门店
     *
     * @param shopStoreVO 前台表单
     * @return
     */
    @PostMapping("/update")
    @ApiOperation("更新门店")
    public String update(@RequestBody ShopStoreVO shopStoreVO) {
        if (!shopService.update(shopStoreVO)) {
            return "failed";
        }
        log.info("更新门店:{} 完成", shopStoreVO.getStoreName());
        return "success";
    }

    /**
     * 下架门店
     *
     * @param storeId 门店id
     * @return
     */
    @PostMapping("/close/{storeId}")
    @ApiOperation("下架门店")
    public Boolean offLine(@PathVariable("storeId") Integer storeId) {
        shopService.offLine(storeId);
        log.info("门店:{} 下架完成", storeId);
        return Boolean.TRUE;
    }

    /**
     * 上架门店
     *
     * @param storeId 门店id
     * @return
     */
    @PostMapping("/open/{storeId}")
    @ApiOperation("上架门店")
    public Boolean open(@PathVariable("storeId") Integer storeId) {
        shopService.onLine(storeId);
        log.info("门店:{} 上架完成", storeId);
        return Boolean.TRUE;
    }

    /**
     * 分页查询门店列表
     *
     * @param pageNum  页数
     * @param pageSize 每页展示数据数量
     * @return
     */
    @GetMapping("/page")
    @ApiOperation("查询门店列表")
    public List<ShopStoreVO> page(Integer pageNum, Integer pageSize) {
        List<ShopStoreVO> shopStoreVoS = shopService.listByPage(pageNum, pageSize);
        return shopStoreVoS;
    }

    /**
     * 查询门店详情
     *
     * @param storeId 门店id
     * @return
     */
    @GetMapping("/{storeId}")
    @ApiOperation("查询门店详情")
    public ShopStoreVO page(@PathVariable("storeId") Integer storeId) {
        ShopStoreVO shopStoreVO = shopService.findById(storeId);
        return shopStoreVO;
    }

    /**
     * 删除门店
     *
     * @param shopStoreVO 前台表单
     * @return
     */
    @PostMapping("/delete")
    @ApiOperation("删除门店")
    public Boolean delete(@RequestBody ShopStoreVO shopStoreVO) {
        shopService.delete(shopStoreVO.getStoreId());
        log.info("删除门店:{} 完成", shopStoreVO.getStoreName());
        return Boolean.TRUE;
    }
}
