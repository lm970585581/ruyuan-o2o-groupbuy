package com.ruyuan.o2o.groupbuy.pay.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 用户消费记录实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class PayUserModel implements Serializable {
    private static final long serialVersionUID = 4829134230054983746L;

    /**
     * 主键id
     */
    private Integer id;

    /**
     * 消费者用户id
     */
    private Integer userId;

    /**
     * 订单id
     */
    private String orderId;

    /**
     * 支付id
     */
    private Integer payId;
}