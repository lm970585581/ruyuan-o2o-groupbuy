package com.ruyuan.o2o.groupbuy.pay.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
 * 用户消费记录VO类
 *
 * @author ming qian
 */
@ApiModel(value = "用户消费记录VO类")
@Getter
@Setter
@ToString
public class PayUserVO implements Serializable {
    private static final long serialVersionUID = -2649629185815427987L;

    /**
     * 主键id
     */
    @ApiModelProperty("主键id")
    private Integer id;

    /**
     * 消费者用户id
     */
    @ApiModelProperty("消费者用户id")
    private Integer userId;

    /**
     * 订单id
     */
    @ApiModelProperty("订单id")
    private String orderId;

    /**
     * 支付id
     */
    @ApiModelProperty("支付id")
    private Integer payId;
}