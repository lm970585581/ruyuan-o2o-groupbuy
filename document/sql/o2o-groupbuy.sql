CREATE database if NOT EXISTS `o2o-groupbuy` default character set utf8mb4 collate utf8mb4_unicode_ci;
use `o2o-groupbuy`;

SET NAMES utf8mb4;

SET
FOREIGN_KEY_CHECKS = 0;
-- 管理后台商户账号表，系统默认有system全局管理员，一个商户有一个管理员账号，管理员id就是商户id
DROP TABLE IF EXISTS `ry_account`;
CREATE TABLE `ry_account`
(
    `admin_id`    int unsigned NOT NULL AUTO_INCREMENT COMMENT '管理用户id',
    `admin_name`  varchar(1024) DEFAULT NULL COMMENT '管理用户名称',
    `mobile`      int           DEFAULT NULL COMMENT '手机号码',
    `passwd`      varchar(1024) DEFAULT NULL COMMENT '密码',
    `role_id`     int           DEFAULT NULL COMMENT '角色id',
    `last_login`  datetime      DEFAULT NULL COMMENT '最后登录时间',
    `last_ip`     varchar(15)   DEFAULT NULL COMMENT '最后登录ip',
    `closed`      int           DEFAULT NULL COMMENT '账号是否已关闭 0正常 1关闭',
    `create_date` datetime      DEFAULT NULL COMMENT '账号创建时间',
    PRIMARY KEY (`admin_id`) USING BTREE
) ENGINE = InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET = utf8
   COMMENT '管理后台商户账号表';

-- 默认增加系统管理员账号
INSERT INTO `ry_account`
VALUES (1, 'system', null, 'system', 1, null, null, 0, null);

-- 角色表
DROP TABLE IF EXISTS `ry_role`;
CREATE TABLE `ry_role`
(
    `role_id`   int unsigned NOT NULL AUTO_INCREMENT COMMENT '角色id',
    `role_name` varchar(1024) DEFAULT NULL COMMENT '角色名称',
    `role_type` int           DEFAULT NULL COMMENT '角色类型 0 system 1 admin 2 store',
    PRIMARY KEY (`role_id`) USING BTREE
) ENGINE=InnoDB
  AUTO_INCREMENT = 2
  DEFAULT CHARSET=utf8  COMMENT '角色表';

-- 默认增加系统管理员角色
INSERT INTO `ry_role`
VALUES (1, '系统维护管理员', 0);

-- 账号角色绑定表
DROP TABLE IF EXISTS `ry_account_role_bind`;
CREATE TABLE `ry_account_role_bind`
(
    `id`       int unsigned AUTO_INCREMENT COMMENT '主键id',
    `admin_id` int COMMENT '用户id',
    `role_id`  int COMMENT '角色id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT '账号角色绑定表';

-- 权限表
DROP TABLE IF EXISTS `ry_auth`;
CREATE TABLE `ry_auth`
(
    `auth_id`     int unsigned NOT NULL AUTO_INCREMENT COMMENT '权限编号',
    `auth_url`    varchar(1024) DEFAULT NULL COMMENT '权限对应的请求URL',
    `auth_name`   varchar(1024) DEFAULT NULL COMMENT '权限名称',
    `parent_id`   int           DEFAULT NULL COMMENT '父权限id',
    `create_time` datetime NOT NULL COMMENT '权限创建时间',
    `update_time` datetime NOT NULL COMMENT '权限更新时间',
    PRIMARY KEY (`auth_id`),
    KEY           `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';

-- 角色权限绑定表
DROP TABLE IF EXISTS `ry_role_auth_bind`;
CREATE TABLE `ry_role_auth_bind`
(
    `id`      int unsigned AUTO_INCREMENT COMMENT '主键id',
    `role_id` int COMMENT '角色id',
    `auth_id` varchar(1024) COMMENT '权限id集合',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8  COMMENT '角色权限绑定表';

-- 省市区关系映射表
DROP TABLE IF EXISTS `ry_address_mapping`;
CREATE TABLE `ry_address_mapping`
(
    `province_id`   int           NOT NULL COMMENT '省id',
    `province_name` varchar(1024) NOT NULL COMMENT '省名称',
    `city_id`       int           NOT NULL COMMENT '市id',
    `city_name`     varchar(1024) NOT NULL COMMENT '市名称',
    `district_id`   int           NOT NULL COMMENT '区id',
    `district_name` varchar(1024) NOT NULL COMMENT '区名称'
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '省市区关系映射表';

-- 门店表，一个商户名下有多个门店
DROP TABLE IF EXISTS `ry_store`;
CREATE TABLE `ry_store`
(
    `store_id`      int unsigned NOT NULL AUTO_INCREMENT COMMENT '门店id',
    `shop_id`       int COMMENT '商户id',
    `store_name`    varchar(1024) COMMENT '门店名称',
    `store_desc`    varchar(1024) COMMENT '门店宣传',
    `check_status`  int COMMENT '门店审核状态 0 已创建 1 审核通过 2 审核拒绝',
    `sale_status`   int COMMENT '销售状态 0 未上架 1 已上架 2 已下架',
    `store_phone`   varchar(1024) COMMENT '门店联系电话',
    `icon`          text COMMENT '门店icon图片地址',
    `province_id`   int COMMENT '省id',
    `city_id`       int COMMENT '市id',
    `district_id`   int COMMENT '区id',
    `store_address` varchar(1024) COMMENT '门店地址',
    `create_time`   datetime COMMENT '创建时间',
    `create_oper`   int COMMENT '创建人',
    `update_time`   datetime COMMENT '更新时间',
    `update_oper`   int COMMENT '更新人',
    `del_flag`      int COMMENT '删除标记 0 未删除 1 已删除',
    PRIMARY KEY (`store_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '门店表';

-- 商品表
-- 正常售卖的是团购商品
-- 积分商品用于提供给用户积分兑换
-- 抽奖商品用于抽奖服务使用
DROP TABLE IF EXISTS `ry_item`;
CREATE TABLE `ry_item`
(
    `item_id`             int unsigned NOT NULL AUTO_INCREMENT COMMENT '商品id',
    `store_id`            int NOT NULL COMMENT '所属门店id',
    `shop_id`             int NOT NULL COMMENT '所属商户id',
    `item_name`           varchar(1024) DEFAULT '' COMMENT '商品名称',
    `item_desc`           varchar(1024) DEFAULT '' COMMENT '商品宣传描述',
    `price`               double COMMENT '团购商品原价',
    `groupbuy_price`      double COMMENT '团购商品优惠价',
    `item_check_status`   int NOT NULL COMMENT '商品销售状态 0 已创建 1 审核通过 2 审核拒绝',
    `sale_status`         int NOT NULL COMMENT '商品上下架状态 0 未上架 1 已上架 2 已下架',
    `item_property`       int NOT NULL COMMENT '商品属性 0 实物 1 虚拟',
    `item_type`           int NOT NULL COMMENT '商品类型 0 团购商品 1 积分商品 2 抽奖商品',
    `lottery_probability` double COMMENT '抽奖商品中奖概率',
    `points_price`        int COMMENT '商品积分价格',
    `item_img1`           text COMMENT '商品图片地址1',
    `item_img2`           text COMMENT '商品图片地址2',
    `item_img3`           text COMMENT '商品图片地址3',
    `item_img4`           text COMMENT '商品图片地址4',
    `item_img5`           text COMMENT '商品图片地址5',
    `item_rich_text`      text COMMENT '商品富文本宣传',
    `item_rule`           text COMMENT '商品使用规则说明',
    `stock`               int COMMENT '商品库存',
    `create_time`         datetime COMMENT '创建时间',
    `create_oper`         int COMMENT '创建人',
    `update_time`         datetime COMMENT '更新时间',
    `update_oper`         int COMMENT '更新人',
    `del_flag`            int COMMENT '删除标记 0 未删除 1 已删除',
    PRIMARY KEY (`item_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '商品表';

-- 活动表
-- 商户创建活动
DROP TABLE IF EXISTS `ry_promotions`;
CREATE TABLE `ry_promotions`
(
    `promotion_id`     int unsigned NOT NULL AUTO_INCREMENT COMMENT '活动id',
    `store_id`         int      NOT NULL COMMENT '所属门店id',
    `shop_id`          int      NOT NULL COMMENT '所属商户id',
    `promotion_name`   varchar(1024) DEFAULT '' COMMENT '活动名称',
    `promotion_desc`   varchar(1024) DEFAULT '' COMMENT '活动宣传描述',
    `promotion_type`   int COMMENT '活动类型 0满减 1折扣',
    `promotion_status` int COMMENT '活动状态 0已创建 1运行中 2已结束 3已中止',
    `full_price`       double COMMENT '满减活动满足金额',
    `deduct_price`     double COMMENT '满减活动扣减金额',
    `discount_price`   double COMMENT '折扣活动消费金额打折比例',
    `create_time`      datetime NOT NULL COMMENT '创建时间',
    `create_oper`      int      NOT NULL COMMENT '创建人',
    `del_flag`         int COMMENT '删除标记 0 未删除 1 已删除',
    PRIMARY KEY (`promotion_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '活动表';

-- 用户积分表
-- 用户消费后可增加积分，积分用于兑换积分商品
DROP TABLE IF EXISTS `ry_user_points`;
CREATE TABLE `ry_user_points`
(
    `id`           int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`      int NOT NULL COMMENT '消费者用户id',
    `points_add`   int NOT NULL COMMENT '本次增加积分',
    `points_total` int NOT NULL COMMENT '总积分',
    `pay_id`       int NOT NULL COMMENT '支付id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '用户积分表';

-- 用户抽奖记录表
-- 用户消费后可参与实际门店下的抽奖，抽奖商品随抽随拿，不支持物流配送
DROP TABLE IF EXISTS `ry_user_lottery`;
CREATE TABLE `ry_user_lottery`
(
    `id`             int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`        int      NOT NULL COMMENT '消费者用户id',
    `pay_id`         int      NOT NULL COMMENT '支付id',
    `item_id`        int      NOT NULL COMMENT '抽奖商品id',
    `store_id`       int      NOT NULL COMMENT '抽奖所在门店id',
    `lottery_result` int      NOT NULL COMMENT '抽奖结果 0 未中奖 1 中奖',
    `create_time`    datetime NOT NULL COMMENT '抽奖时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '用户抽奖记录表';

-- 优惠券表
-- 优惠券可到店领取，也可通过短信远程推送
DROP TABLE IF EXISTS `ry_coupons`;
CREATE TABLE `ry_coupons`
(
    `id`                     int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `coupons_id`             int COMMENT '优惠券id',
    `coupons_name`           int COMMENT '优惠券名称',
    `coupons_type`           int COMMENT '优惠券类型 0 满减 1 折扣',
    `coupons_full_price`     double COMMENT '满减券满足金额',
    `coupons_deduct_price`   double COMMENT '满减券扣减金额',
    `coupons_discount_price` double COMMENT '折扣券打折比例',
    `coupons_start_time`     datetime NOT NULL COMMENT '优惠券有效开始时间',
    `coupons_end_time`       datetime NOT NULL COMMENT '优惠券有效结束时间',
    `create_time`            datetime NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '优惠券表';

-- 用户领取优惠券记录表
DROP TABLE IF EXISTS `ry_user_coupons_receive`;
CREATE TABLE `ry_user_coupons_receive`
(
    `id`             int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `coupons_id`     int COMMENT '优惠券id',
    `user_id`        int COMMENT '用户id',
    `coupons_status` int COMMENT '优惠券状态 0 未使用 1 已使用 2 已过期',
    `receive_time`   datetime COMMENT '领取时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '用户领取优惠券记录表';

-- 订单表
-- 订单分为到店消费订单和物流配送订单
-- 到店消费订单生成二维码和数字券码，用于进店展示
-- 配送订单可由门店人员配送或发送物流快递配送
DROP TABLE IF EXISTS `ry_order`;
CREATE TABLE `ry_order`
(
    `id`            int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `order_id`      VARCHAR(1024) NOT NULL COMMENT '订单id',
    `user_id`       int           NOT NULL COMMENT '消费者用户id',
    `store_id`      int           NOT NULL COMMENT '门店id',
    `shop_id`       int           NOT NULL COMMENT '商户id',
    `item_id`       int           NOT NULL COMMENT '商品id',
    `item_num`      int           NOT NULL COMMENT '商品购买数量',
    `promotions_id` int           NOT NULL COMMENT '活动id',
    `coupons_id`    int           NOT NULL COMMENT '优惠券id',
    `order_price`   double        NOT NULL COMMENT '订单实际金额',
    `order_type`    int           NOT NULL COMMENT '订单类型 0 到店消费 1配送',
    `order_status`  int           NOT NULL COMMENT '订单状态 10 待付款 11 已超时 12 支付成功 13 支付失败 14 已发货 15 已签收 16 拒绝签收 17 无人签收 18 订单取消',
    `create_time`   datetime      NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '订单表';

-- 支付表
DROP TABLE IF EXISTS `ry_pay`;
CREATE TABLE `ry_pay`
(
    `id`          int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `order_id`    VARCHAR(1024) NOT NULL COMMENT '订单id',
    `pay_id`      int           NOT NULL COMMENT '支付id',
    `user_id`     int           NOT NULL COMMENT '消费者用户id',
    `store_id`    int           NOT NULL COMMENT '门店id',
    `pay_status`  int           NOT NULL COMMENT '支付状态 0 支付成功 1 支付失败',
    `pay_channel` int           NOT NULL COMMENT '支付渠道 0 会员余额 1 银联卡 2 支付宝 3 微信',
    `pay_price`   double        NOT NULL COMMENT '支付金额',
    `pay_time`    datetime      NOT NULL COMMENT '支付时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '支付表';

-- 用户消费记录表
DROP TABLE IF EXISTS `ry_user_pay`;
CREATE TABLE `ry_user_pay`
(
    `id`       int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`  int           NOT NULL COMMENT '消费者用户id',
    `order_id` VARCHAR(1024) NOT NULL COMMENT '订单id',
    `pay_id`   int           NOT NULL COMMENT '支付id',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '用户消费记录表';

-- 短信模板表
-- 商户可自定义短信模板，用于精准发送给用户短信
DROP TABLE IF EXISTS `ry_message_template`;
CREATE TABLE `ry_message_template`
(
    `message_id`            int unsigned NOT NULL AUTO_INCREMENT COMMENT '短信id',
    `message_template`      text COMMENT '短信模板内容',
    `message_template_type` int COMMENT '短信模板类型 0 提醒支付短信 1 订单支付完成短信 2 物流配送短信 3 门店核销券码短信 4 活动上新短信 5 优惠券过期提醒短信',
    `create_time`           datetime COMMENT '创建时间',
    `create_oper`           int COMMENT '创建人',
    PRIMARY KEY (`message_id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '短信模板表';

-- 消费券码记录表
-- 到店消费的券码
DROP TABLE IF EXISTS `ry_user_code`;
CREATE TABLE `ry_user_code`
(
    `id`           int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`      int      NOT NULL COMMENT '消费者用户id',
    `pay_id`       int      NOT NULL COMMENT '支付id',
    `store_id`     int      NOT NULL COMMENT '门店id',
    `qr_code_img`  text     NOT NULL COMMENT '二维码地址',
    `digital_code` int      NOT NULL COMMENT '数字券码',
    `code_status`  int      NOT NULL COMMENT '券码状态 0 未核销 1 已核销',
    `create_time`  datetime NOT NULL COMMENT '创建时间',
    `update_time`  datetime NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '消费券码记录表';

-- 消费者用户表
DROP TABLE IF EXISTS `ry_user`;
CREATE TABLE `ry_user`
(
    `id`            int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`       int           NOT NULL COMMENT '消费者用户id',
    `user_phone`    varchar(1024) NOT NULL COMMENT '用户手机号',
    `store_id`      int           NOT NULL COMMENT '门店id',
    `member_status` int           NOT NULL COMMENT '会员状态 0 未充值注册成为会员 1 已充值注册成为会员',
    `create_time`   datetime COMMENT '创建时间',
    `update_time`   datetime COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '消费者用户表';

-- 默认增加模拟消费者用户
INSERT INTO `ry_user`
VALUES (1, 1, '15811112222', 1, 0, null, null);

-- 消费者用户充值余额表
-- 用户可于门店中充值，充值后默认成为门店会员
DROP TABLE IF EXISTS `ry_user_overage`;
CREATE TABLE `ry_user_overage`
(
    `id`            int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`       int      NOT NULL COMMENT '消费者用户id',
    `store_id`      int      NOT NULL COMMENT '门店id',
    `overage_add`   double   NOT NULL COMMENT '消费者在门店本次充值余额',
    `overage_total` double   NOT NULL COMMENT '消费者在门店总余额',
    `create_time`   datetime NOT NULL COMMENT '创建时间',
    `update_time`   datetime NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '消费者用户充值余额表';

-- 消费者用户操作记录流水表
-- 模拟用户埋点，可在不同位置下保存操作记录
DROP TABLE IF EXISTS `ry_user_operate_record`;
CREATE TABLE `ry_user_operate_record`
(
    `id`             int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`        int      NOT NULL COMMENT '消费者用户id',
    `store_id`       int      NOT NULL COMMENT '门店id',
    `operate_record` int      NOT NULL COMMENT '操作记录 0 下单 1 支付完成 2 到店核销 3 货物签收 4 投诉门店 5 获赠积分 6 参与抽奖',
    `create_time`    datetime NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '消费者用户操作记录流水表';

-- 消费者用户投诉表
-- 用户投诉门店
DROP TABLE IF EXISTS `ry_user_complaints`;
CREATE TABLE `ry_user_complaints`
(
    `id`                 int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`            int      NOT NULL COMMENT '消费者用户id',
    `store_id`           int      NOT NULL COMMENT '门店id',
    `user_phone`         int      NOT NULL COMMENT '用户手机号',
    `complaints_content` text     NOT NULL COMMENT '投诉内容',
    `process_status`     int      NOT NULL COMMENT '处理状态 0 未处理 1 已回复 2 已解决 3 未解决',
    `process_content`    text     NOT NULL COMMENT '解决方式',
    `create_time`        datetime NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '消费者用户投诉表';

-- 消费者用户评价表
-- 评价门店商品
DROP TABLE IF EXISTS `ry_user_evaluate`;
CREATE TABLE `ry_user_evaluate`
(
    `id`          int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`     int      NOT NULL COMMENT '消费者用户id',
    `store_id`    int      NOT NULL COMMENT '门店id',
    `item_id`     int      NOT NULL COMMENT '商品id',
    `level`       int      NOT NULL COMMENT '评价等级 1 一星 2 二星 3 三星 4 四星 5 五星',
    `create_time` datetime NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '消费者用户评价表';

-- 消费者收货地址表
DROP TABLE IF EXISTS `ry_user_shipping_address`;
CREATE TABLE `ry_user_shipping_address`
(
    `id`             int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `user_id`        int      NOT NULL COMMENT '消费者用户id',
    `province_id`    int      NOT NULL COMMENT '省id',
    `city_id`        int      NOT NULL COMMENT '市id',
    `district_id`    int      NOT NULL COMMENT '区id',
    `detail_address` text     NOT NULL COMMENT '详细地址',
    `create_time`    datetime NOT NULL COMMENT '创建时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '消费者收货地址表';

-- 门店物流配送表
-- 门店工作人员操作，更新配送物流信息
DROP TABLE IF EXISTS `ry_logistics`;
CREATE TABLE `ry_logistics`
(
    `id`                          int unsigned NOT NULL AUTO_INCREMENT COMMENT '主键id',
    `store_id`                    int           NOT NULL COMMENT '门店id',
    `user_id`                     int           NOT NULL COMMENT '消费者用户id',
    `order_id`                    VARCHAR(1024) NOT NULL COMMENT '订单id',
    `pay_id`                      int           NOT NULL COMMENT '支付id',
    `consignee_name`              int           NOT NULL COMMENT '收货人昵称',
    `consignee_phone`             int           NOT NULL COMMENT '收货人手机号',
    `consignee_address`           text          NOT NULL COMMENT '收货人地址配送',
    `order_logistics_type`        int           NOT NULL COMMENT '订单物流状态 0 门店自配送 1 物流配送',
    `delivery_name`               varchar(1024) NOT NULL COMMENT '门店送货人',
    `delivery_phone`              int           NOT NULL COMMENT '门店送货人手机号',
    `logistics_name`              varchar(1024) NOT NULL COMMENT '物流公司名称',
    `logistics_no`                int           NOT NULL COMMENT '物流单号',
    `order_logistics_result`      int           NOT NULL COMMENT '订单物流状态 0 已配送 1 配送完成 2 配送失败',
    `order_logistics_failed_info` text          NOT NULL COMMENT '配送失败原因',
    `create_time`                 datetime      NOT NULL COMMENT '创建时间',
    `update_time`                 datetime      NOT NULL COMMENT '更新时间',
    PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8
   COMMENT '门店物流配送表';