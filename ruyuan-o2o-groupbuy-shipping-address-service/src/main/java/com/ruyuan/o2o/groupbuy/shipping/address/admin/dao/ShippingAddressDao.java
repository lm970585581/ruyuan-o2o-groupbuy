package com.ruyuan.o2o.groupbuy.shipping.address.admin.dao;

import com.ruyuan.o2o.groupbuy.shipping.address.model.ShippingAddressModel;
import org.springframework.stereotype.Repository;

/**
 * 用户收货地址服务DAO
 *
 * @author ming qian
 */
@Repository
public interface ShippingAddressDao {

    /**
     * 保存用户收货地址
     *
     * @param shippingAddressModel
     */
    void save(ShippingAddressModel shippingAddressModel);

}