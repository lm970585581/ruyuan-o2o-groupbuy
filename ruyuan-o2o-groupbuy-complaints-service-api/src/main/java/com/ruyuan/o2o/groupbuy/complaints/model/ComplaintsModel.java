package com.ruyuan.o2o.groupbuy.complaints.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 用户投诉门店服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class ComplaintsModel implements Serializable {
    private static final long serialVersionUID = 2490096173026291308L;

    /**
     * 主键id
     */
    private Integer id;
    /**
     * 消费者用户id
     */
    private Integer userId;
    /**
     * 门店id
     */
    private Integer storeId;
    /**
     * 用户手机号
     */
    private Integer userPhone;
    /**
     * 处理状态 0 未处理 1 已回复 2 已解决 3 未解决
     */
    private Integer processStatus;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 投诉内容
     */
    private String complaintsContent;
    /**
     * 解决方式
     */
    private String processContent;
}