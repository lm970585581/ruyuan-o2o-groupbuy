package com.ruyuan.o2o.groupbuy.message.admin.dao;

import com.ruyuan.o2o.groupbuy.message.model.MessageTemplateModel;
import org.springframework.stereotype.Repository;

/**
 * 短信模板DAO
 *
 * @author ming qian
 */
@Repository
public interface MessageTemplateDao {

    /**
     * 保存短信模板
     *
     * @param messageTemplateModel
     * @return
     */
    void saveMessageTemplate(MessageTemplateModel messageTemplateModel);
}