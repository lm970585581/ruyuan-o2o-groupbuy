package com.ruyuan.o2o.groupbuy.order.admin.service;

import com.github.pagehelper.PageHelper;
import com.ruyuan.o2o.groupbuy.common.BeanMapper;
import com.ruyuan.o2o.groupbuy.common.TimeUtil;
import com.ruyuan.o2o.groupbuy.order.admin.dao.OrderDao;
import com.ruyuan.o2o.groupbuy.order.dto.OrderExcelDTO;
import com.ruyuan.o2o.groupbuy.order.model.OrderModel;
import com.ruyuan.o2o.groupbuy.order.service.OrderService;
import com.ruyuan.o2o.groupbuy.order.vo.OrderBuyVO;
import com.ruyuan.o2o.groupbuy.order.vo.OrderVO;
import com.ruyuan.o2o.groupbuy.record.service.RecordService;
import com.ruyuan.o2o.groupbuy.record.vo.RecordVO;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * 处理订单服务的增删改查service组件
 *
 * @author ming qian
 */
@Service(version = "1.0.0", interfaceClass = OrderService.class, cluster = "failfast", loadbalance = "roundrobin")
public class OrderServiceImpl implements OrderService {

    /**
     * 订单状态：待付款
     */
    private static final Integer ORDER_PENDING_PAYMENT = 10;

    @Reference(version = "1.0.0", interfaceClass = RecordService.class,
            cluster = "failfast", loadbalance = "roundrobin")
    private RecordService recordService;

    @Autowired
    private OrderDao orderDao;

    /**
     * 创建订单
     * 此处可添加对于活动、优惠券、各种优惠金额的校验
     * 例如：活动和优惠券不可重复，在满减、折扣等基础上计算优惠金额等
     *
     * @param orderBuyVO
     */
    @Override
    public void save(OrderBuyVO orderBuyVO) {
        OrderModel orderModel = new OrderModel();
        BeanMapper.copy(orderBuyVO, orderModel);

        orderModel.setCreateTime(TimeUtil.format(System.currentTimeMillis()));
        orderModel.setOrderId(String.valueOf(UUID.randomUUID()));
        orderModel.setOrderStatus(ORDER_PENDING_PAYMENT);

        orderDao.save(orderModel);

        //  保存用户记录
        //  模拟在创建订单时记录用户行为
        RecordVO recordVO = new RecordVO();
        recordVO.setStoreId(orderBuyVO.getStoreId());
        recordVO.setUserId(orderBuyVO.getUserId());
        recordVO.setOperateRecord(0);
        recordVO.setCreateTime(TimeUtil.format(System.currentTimeMillis()));

        recordService.save(recordVO);
    }

    /**
     * 分页查询订单
     *
     * @return
     */
    @Override
    public List<OrderVO> listByPage(Integer pageNum, Integer pageSize) {
        List<OrderVO> orderVoS = new ArrayList<>();

        PageHelper.startPage(pageNum, pageSize);
        List<OrderModel> orderModels = orderDao.listByPage();

        for (OrderModel orderModel : orderModels) {
            OrderVO orderVO = new OrderVO();
            BeanMapper.copy(orderModel, orderVO);
            orderVoS.add(orderVO);
        }

        return orderVoS;
    }

    /**
     * 根据id查询订单
     *
     * @param orderId 订单id
     * @return
     */
    @Override
    public OrderVO findById(String orderId) {
        OrderModel orderModel = orderDao.findById(orderId);
        OrderVO orderVO = new OrderVO();
        BeanMapper.copy(orderModel, orderVO);
        return orderVO;
    }

    /**
     * 查询所有数据
     *
     * @return
     */
    @Override
    public List<OrderExcelDTO> selectAll() {
        List<OrderExcelDTO> orderExcelDTOList = new ArrayList<>();
        OrderExcelDTO orderExcelDTO = new OrderExcelDTO();

        List<OrderModel> orderModels = orderDao.listByPage();
        for (OrderModel orderModel : orderModels) {
            BeanMapper.copy(orderModel, orderExcelDTO);
            orderExcelDTOList.add(orderExcelDTO);
        }
        return orderExcelDTOList;
    }
}
