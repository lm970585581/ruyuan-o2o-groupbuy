package com.ruyuan.o2o.groupbuy.code.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.util.Date;

/**
 * 券码服务实体类
 *
 * @author ming qian
 */
@Getter
@Setter
@ToString
public class CodeModel implements Serializable {
    private static final long serialVersionUID = 2127270264928997275L;

    /**
     * 主键id
     */
    private Integer id;
    /**
     * 消费者用户id
     */
    private Integer userId;
    /**
     * 支付id
     */
    private Integer payId;
    /**
     * 门店id
     */
    private Integer storeId;
    /**
     * 数字券码
     */
    private Integer digitalCode;
    /**
     * 券码状态 0 未核销 1 已核销
     */
    private Integer codeStatus;
    /**
     * 创建时间
     */
    private String createTime;
    /**
     * 更新时间
     */
    private String updateTime;
    /**
     * 二维码地址
     */
    private String qrCodeImg;
}